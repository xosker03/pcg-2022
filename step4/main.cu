/**
 * @File main.cu
 *
 * The main file of the project
 *
 * Paralelní programování na GPU (PCG 2021)
 * Projekt c. 1 (cuda)
 * Login: xosker03
 */

#include <sys/time.h>
#include <cstdio>
#include <cmath>

#include "nbody.h"
#include "h5Helper.h"

#include <mutex>
#include <thread>

/**
 * Main rotine
 * @param argc
 * @param argv
 * @return
 */

cudaStream_t stream_M;
cudaStream_t stream_GW;
cudaStream_t stream_C;
std::mutex stream_HG_1;
std::mutex stream_HG_2;
std::mutex stream_HM_1;
std::mutex stream_HM_2;

std::mutex mutex_load;
std::mutex mutex_final;

volatile int temp_s_G;
volatile int temp_s_H;

MemDesc* global_md;
float4 comOnGPU;
float* com;

void data_handler(char* argv8, char* argv9)
{
	H5Helper h5Helper(argv8, argv9, *global_md);

	try {
		h5Helper.init();
		h5Helper.readParticleData();
	}
	catch (const std::exception& e) {
		std::cerr<<e.what()<<std::endl;
		return;
	}
	mutex_load.unlock();
	
	while(1){
		if(stream_HG_1.try_lock()) {
			cudaStreamSynchronize(stream_GW);
			h5Helper.writeParticleData(temp_s_G);
			stream_HG_2.unlock();
		}
		if(stream_HM_1.try_lock()) {
			cudaStreamSynchronize(stream_M);
			h5Helper.writeCom(com[0] / com[3], com[1] / com[3], com[2] / com[3], com[3], temp_s_H);	
			stream_HM_2.unlock();
		}
		if(mutex_final.try_lock()) {
			h5Helper.writeComFinal(comOnGPU.x, comOnGPU.y, comOnGPU.z, comOnGPU.w);
			h5Helper.writeParticleDataFinal();
			return;
		}
	}
}


int main(int argc, char **argv)
{
  // Time measurement
  struct timeval t1, t2;

  if (argc != 10)
  {
    printf("Usage: nbody <N> <dt> <steps> <threads/block> <write intesity> <reduction threads> <reduction threads/block> <input> <output>\n");
    exit(1);
  }

  // Number of particles
  const int N           = std::stoi(argv[1]);
  // Length of time step
  const float dt        = std::stof(argv[2]);
  // Number of steps
  const int steps       = std::stoi(argv[3]);
  // Number of thread blocks
  const int thr_blc     = std::stoi(argv[4]);
  // Write frequency
  int writeFreq         = std::stoi(argv[5]);
  // number of reduction threads
  const int red_thr     = std::stoi(argv[6]);
  // Number of reduction threads/blocks
  const int red_thr_blc = std::stoi(argv[7]);

  // Size of the simulation CUDA gird - number of blocks
  const size_t simulationGrid = (N + thr_blc - 1) / thr_blc;
  // Size of the reduction CUDA grid - number of blocks
  const size_t reductionGrid  = (red_thr + red_thr_blc - 1) / red_thr_blc;

  // Log benchmark setup
  printf("N: %d\n", N);
  printf("dt: %f\n", dt);
  printf("steps: %d\n", steps);
  printf("threads/block: %d\n", thr_blc);
  printf("blocks/grid: %lu\n", simulationGrid);
  printf("reduction threads/block: %d\n", red_thr_blc);
  printf("reduction blocks/grid: %lu\n", reductionGrid);

  const size_t recordsNum = (writeFreq > 0) ? (steps + writeFreq - 1) / writeFreq : 0;
  writeFreq = (writeFreq > 0) ?  writeFreq : 0;


  t_particles particles_cpu;

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //                            FILL IN: CPU side memory allocation (step 0)                                          //
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	particles_cpu.pos_x = (float *) malloc(N * sizeof(float));
	particles_cpu.pos_y = (float *) malloc(N * sizeof(float));
	particles_cpu.pos_z = (float *) malloc(N * sizeof(float));
	particles_cpu.vel_x = (float *) malloc(N * sizeof(float));
	particles_cpu.vel_y = (float *) malloc(N * sizeof(float));
	particles_cpu.vel_z = (float *) malloc(N * sizeof(float));
	particles_cpu.weight = (float *) malloc(N * sizeof(float));

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //                              FILL IN: memory layout descriptor (step 0)                                          //
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /*
   * Caution! Create only after CPU side allocation
   * parameters:
   *                      Stride of two               Offset of the first
   *  Data pointer        consecutive elements        element in floats,
   *                      in floats, not bytes        not bytes
  */
  MemDesc md(
        particles_cpu.pos_x,	1, 0,              // Postition in X
        particles_cpu.pos_y,	1, 0,              // Postition in Y
        particles_cpu.pos_z,	1, 0,              // Postition in Z
        particles_cpu.vel_x,	1, 0,              // Velocity in X
        particles_cpu.vel_y,	1, 0,              // Velocity in Y
        particles_cpu.vel_z,	1, 0,              // Velocity in Z
        particles_cpu.weight,	1, 0,              // Weight
        N,										   // Number of particles
        recordsNum);                               // Number of records in output file

  // Initialisation of helper class and loading of input data
  /*H5Helper h5Helper(argv[8], argv[9], md);

  try {
		h5Helper.init();
		h5Helper.readParticleData();
  }
  catch (const std::exception& e) {
	std::cerr<<e.what()<<std::endl;
    return -1;
  }*/

	global_md = &md;
	
	stream_HG_1.lock();
	stream_HM_1.lock();
	mutex_load.lock();
	mutex_final.lock();
	std::thread th1(data_handler, argv[8], argv[9]);
	mutex_load.lock();

	t_particles particles_gpu;
	t_particles particles_gpu_2;
	t_particles* p_particles_gpu = &particles_gpu;
	t_particles* p_particles_gpu_2 = &particles_gpu_2;
	t_particles* p_particles_gpu_temp;

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //                                  FILL IN: GPU side memory allocation (step 0)                                    //
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	for (int i = 0; i < 7; ++i)
		cudaMalloc(&((float**)&particles_gpu)[i], N * sizeof(float));
	for (int i = 0; i < 7; ++i)
		cudaMalloc(&((float**)&particles_gpu_2)[i], N * sizeof(float));
	
	//for (int i = 0; i < 3; ++i)
	//	cudaMalloc(&((float**)&velociraptor)[i], N * sizeof(float));


  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //                                       FILL IN: memory transfers (step 0)                                         //
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	for (int i = 0; i < 7; ++i)
		cudaMemcpy(((float**)&particles_gpu)[i], ((float**)&particles_cpu)[i], N * sizeof(float), cudaMemcpyHostToDevice);
	for (int i = 0; i < 7; ++i)
		cudaMemcpy(((float**)&particles_gpu_2)[i], ((float**)&particles_cpu)[i], N * sizeof(float), cudaMemcpyHostToDevice);


  gettimeofday(&t1, 0);
	  
	dim3 blockDim(thr_blc);
	dim3 gridDim(simulationGrid);
	dim3 mass_blockDim(red_thr_blc);
	dim3 mass_gridDim(red_thr / red_thr_blc);
	

	com = (float *) calloc(4, sizeof(float));
	float* com_g;
	int* lock_g;
	cudaMalloc(&com_g, 4 * sizeof(float));
	cudaMalloc(&lock_g, 1 * sizeof(int));


	cudaStreamCreate(&stream_M);
	cudaStreamCreate(&stream_GW);
	cudaStreamCreate(&stream_C);
	
	

  for(int s = 0; s < steps; s++)
  {
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                       FILL IN: kernels invocation (step 0)                                     //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if (writeFreq > 0 && ((s) % writeFreq == 0)) {
		//STAGE GWRITE, SYNC HDFG
		stream_HG_2.lock();
		temp_s_G = s/writeFreq;
		for (int i = 0; i < 7; ++i)
			cudaMemcpyAsync(((float**)&particles_cpu)[i], ((float**)p_particles_gpu)[i], N * sizeof(float), cudaMemcpyDeviceToHost, stream_GW);
		stream_HG_1.unlock();
		//STAGE HDFG
	}
	
	//STAGE COMPUTE
	calculate_velocity<<<gridDim, blockDim, thr_blc * sizeof(float) * 8 * 2, stream_C>>>(*p_particles_gpu, *p_particles_gpu_2, N, dt);
	
	//SYNC: MASS, GWRITE, COMPUTE
	cudaStreamSynchronize(stream_M);
	cudaStreamSynchronize(stream_GW);
	cudaStreamSynchronize(stream_C);
	
	p_particles_gpu_temp = p_particles_gpu;
	p_particles_gpu = p_particles_gpu_2;
	p_particles_gpu_2 = p_particles_gpu_temp;
	
	if (writeFreq > 0 && (s % writeFreq == 0)) {
		//STAGE MASS
		cudaMemsetAsync(com_g, 0, 4 * sizeof(float), stream_M);
		centerOfMass<<<mass_gridDim, mass_blockDim, red_thr_blc * sizeof(float) * (4+1), stream_M>>>(*p_particles_gpu, com_g + 0, com_g + 1, com_g + 2, com_g + 3, lock_g, N);
		//STAGE MWRITE, SYNC HDFM
		stream_HM_2.lock();
		temp_s_H = s/writeFreq;
		cudaMemcpyAsync(com, com_g, 4 * sizeof(float), cudaMemcpyDeviceToHost, stream_M);
		stream_HM_1.unlock();
		//STAGE HDFM
    }
  }


  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //              FILL IN: invocation of center-of-mass kernel (step 3.1, step 3.2, step 4)                           //
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  cudaDeviceSynchronize();

  gettimeofday(&t2, 0);

  // Approximate simulation wall time
  double t = (1000000.0 * (t2.tv_sec - t1.tv_sec) + t2.tv_usec - t1.tv_usec) / 1000000.0;
  printf("Time: %f s\n", t);


  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //                             FILL IN: memory transfers for particle data (step 0)                                 //
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	for (int i = 0; i < 7; ++i)
		cudaMemcpy(((float**)&particles_cpu)[i], ((float**)&particles_gpu)[i], N * sizeof(float), cudaMemcpyDeviceToHost);

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //                        FILL IN: memory transfers for center-of-mass (step 3.1, step 3.2)                         //
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  float4 comOnCPU = centerOfMassCPU(md);
 
	cudaMemset(com_g, 0, 4 * sizeof(float));
	centerOfMass<<<mass_gridDim, mass_blockDim, red_thr_blc * sizeof(float) * (4+1)>>>(*p_particles_gpu, com_g + 0, com_g + 1, com_g + 2, com_g + 3, lock_g, N);
	cudaMemcpy(com, com_g, 4 * sizeof(float), cudaMemcpyDeviceToHost);

	comOnGPU.w = com[3];
	comOnGPU.x = com[0] / comOnGPU.w;
	comOnGPU.y = com[1] / comOnGPU.w;
	comOnGPU.z = com[2] / comOnGPU.w;


  std::cout << "Center of mass on CPU:" << std::endl
            << comOnCPU.x <<", "
            << comOnCPU.y <<", "
            << comOnCPU.z <<", "
            << comOnCPU.w
            << std::endl;

  std::cout << "Center of mass on GPU:" << std::endl
            << comOnGPU.x<<", "
            << comOnGPU.y<<", "
            << comOnGPU.z<<", "
            << comOnGPU.w
            << std::endl;

  // Writing final values to the file
	mutex_final.unlock();
 // h5Helper.writeComFinal(comOnGPU.x, comOnGPU.y, comOnGPU.z, comOnGPU.w);
  //h5Helper.writeParticleDataFinal();

	th1.join();

  return 0;
}// end of main
//----------------------------------------------------------------------------------------------------------------------
