/**
 * @File nbody.cu
 *
 * Implementation of the N-Body problem
 *
 * Paralelní programování na GPU (PCG 2021)
 * Projekt c. 1 (cuda)
 * Login: xosker03
 */

#include <cmath>
#include <cfloat>
#include "nbody.h"


/**
 * CUDA kernel to calculate gravitation velocity
 * @param p       - particles
 * @param tmp_vel - temp array for velocities
 * @param N       - Number of particles
 * @param dt      - Size of the time step
 */

__global__ void calculate_gravitation_velocity(t_particles p, t_velocities tmp_vel, int N, float dt)
{/*
	size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    
	float r, dx, dy, dz;
    float vx, vy, vz;
    float r3, G_dt_r3, Fg_dt_m2_r;

	float p1[3];


	if(idx < N){
		p1[0] = ((float**)&p)[0][idx];
		p1[1] = ((float**)&p)[1][idx];
		p1[2] = ((float**)&p)[2][idx];
		vx = 0;
		vy = 0;
		vz = 0;

		for(int i = 0; i < N; ++i){
			dx = p1[POS_X] - p.pos_x[i];
			dy = p1[POS_Y] - p.pos_y[i];
			dz = p1[POS_Z] - p.pos_z[i];
			r = sqrt(dx*dx + dy*dy + dz*dz);
			
			if(r > COLLISION_DISTANCE) {
				r3 = r * r * r + FLT_MIN;
				G_dt_r3 = -G * dt / r3;
				Fg_dt_m2_r = G_dt_r3 * p.weight[i];

				vx += Fg_dt_m2_r * dx;
				vy += Fg_dt_m2_r * dy;
				vz += Fg_dt_m2_r * dz;
			}
		}
		tmp_vel.x[idx] = vx;
		tmp_vel.y[idx] = vy;
		tmp_vel.z[idx] = vz;
	}
*/	
}// end of calculate_gravitation_velocity
//----------------------------------------------------------------------------------------------------------------------

/**
 * CUDA kernel to calculate collision velocity
 * @param p       - particles
 * @param tmp_vel - temp array for velocities
 * @param N       - Number of particles
 * @param dt      - Size of the time step
 */
__global__ void calculate_collision_velocity(t_particles p, t_velocities tmp_vel, int N, float dt)
{/*
	size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    float r, dx, dy, dz;
    float vx, vy, vz;
	
	float p1[7];
	float p2[7];

	if(idx < N){
		p1[0] = ((float**)&p)[0][idx];
		p1[1] = ((float**)&p)[1][idx];
		p1[2] = ((float**)&p)[2][idx];
		p1[3] = ((float**)&p)[3][idx];
		p1[4] = ((float**)&p)[4][idx];
		p1[5] = ((float**)&p)[5][idx];
		p1[6] = ((float**)&p)[6][idx];
		vx = p1[VEL_X];
		vy = p1[VEL_Y];
		vz = p1[VEL_Z];

		for(int i = 0; i < N; ++i){
			p2[0] = ((float**)&p)[0][idx];
			p2[1] = ((float**)&p)[1][idx];
			p2[2] = ((float**)&p)[2][idx];
			p2[3] = ((float**)&p)[3][idx];
			p2[4] = ((float**)&p)[4][idx];
			p2[5] = ((float**)&p)[5][idx];
			p2[6] = ((float**)&p)[6][idx];
			dx = p1[POS_X] - p2[POS_X];
			dy = p1[POS_Y] - p2[POS_Y];
			dz = p1[POS_Z] - p2[POS_Z];
			r = dx*dx + dy*dy + dz*dz;
			if (r > 0.0f && r < COLLISION_DISTANCE_2) {
				vx += ((p1[WEIGHT]* p1[VEL_X] - p2[WEIGHT] *p1[VEL_X] + 2* p2[WEIGHT]* p2[VEL_X]) /
						(p1[WEIGHT] + p2[WEIGHT])) - p1[VEL_X] ;
				vy += ((p1[WEIGHT]* p1[VEL_Y] - p2[WEIGHT] *p1[VEL_Y] + 2* p2[WEIGHT]* p2[VEL_Y]) /
						(p1[WEIGHT] + p2[WEIGHT])) - p1[VEL_Y] ;
				vz += ((p1[WEIGHT]* p1[VEL_Z] - p2[WEIGHT] *p1[VEL_Z] + 2* p2[WEIGHT]* p2[VEL_Z]) /
						(p1[WEIGHT] + p2[WEIGHT])) - p1[VEL_Z] ;
			}
		}
		tmp_vel.x[idx] += vx;
		tmp_vel.y[idx] += vy;
		tmp_vel.z[idx] += vz;
	}
*/
}// end of calculate_collision_velocity
//----------------------------------------------------------------------------------------------------------------------

/**
 * CUDA kernel to update particles
 * @param p       - particles
 * @param tmp_vel - temp array for velocities
 * @param N       - Number of particles
 * @param dt      - Size of the time step
 */
__global__ void update_particle(t_particles p, t_velocities tmp_vel, int N, float dt)
{/*
	size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    float vx, vy, vz;

	if(idx < N){
		vx = tmp_vel.x[idx];
		vy = tmp_vel.y[idx];
		vz = tmp_vel.z[idx];
		p.vel_x[idx] = vx;
		p.vel_y[idx] = vy;
		p.vel_z[idx] = vz;
		p.pos_x[idx] += dt * vx;
		p.pos_y[idx] += dt * vy;
		p.pos_z[idx] += dt * vz;
	}*/
}// end of update_particle
//----------------------------------------------------------------------------------------------------------------------
__global__ void calculate_velocity(t_particles p, t_particles pout, int N, float dt)
{
	const size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
	const int dim = blockDim.x;
	//const int bx = blockIdx.x;
	const int tx = threadIdx.x;
	//const int col = bx * blockDim.x + tx;

	float r, dx, dy, dz;
    float vx, vy, vz;
    float vx2, vy2, vz2;
    float r3, G_dt_r3, Fg_dt_m2_r;
	
	//float p1[7];
	//float p2[7];

	extern __shared__ float sp1[][7 + 1];
	float (*sp2)[7 + 1] = (float (*)[7 + 1]) (((float*)sp1) + dim * 8);
	
	if(idx < N){
		sp1[tx][0] = ((float**)&p)[0][idx];
		sp1[tx][1] = ((float**)&p)[1][idx];
		sp1[tx][2] = ((float**)&p)[2][idx];
		sp1[tx][3] = ((float**)&p)[3][idx];
		sp1[tx][4] = ((float**)&p)[4][idx];
		sp1[tx][5] = ((float**)&p)[5][idx];
		sp1[tx][6] = ((float**)&p)[6][idx];
		__syncthreads();

		vx = 0;
		vy = 0;
		vz = 0;
		
		vx2 = sp1[tx][VEL_X];
		vy2 = sp1[tx][VEL_Y];
		vz2 = sp1[tx][VEL_Z];

		
		for(int m = 0; m < (N-1) / dim + 1; ++m) {
			sp2[tx][0] = ((float**)&p)[0][m * dim + tx];
			sp2[tx][1] = ((float**)&p)[1][m * dim + tx];
			sp2[tx][2] = ((float**)&p)[2][m * dim + tx];
			sp2[tx][3] = ((float**)&p)[3][m * dim + tx];
			sp2[tx][4] = ((float**)&p)[4][m * dim + tx];
			sp2[tx][5] = ((float**)&p)[5][m * dim + tx];
			sp2[tx][6] = ((float**)&p)[6][m * dim + tx];
			__syncthreads();

			for(int i = 0; i < dim; ++i) {
				dx = sp1[tx][POS_X] - sp2[i][POS_X];
				dy = sp1[tx][POS_Y] - sp2[i][POS_Y];
				dz = sp1[tx][POS_Z] - sp2[i][POS_Z];
				r = sqrt(dx*dx + dy*dy + dz*dz);
			
				if(r > COLLISION_DISTANCE) {
					r3 = r * r * r + FLT_MIN;
					G_dt_r3 = -G * dt / r3;
					Fg_dt_m2_r = G_dt_r3 * sp2[i][WEIGHT];

					vx += Fg_dt_m2_r * dx;
					vy += Fg_dt_m2_r * dy;
					vz += Fg_dt_m2_r * dz;
				}

				if (r > 0.0f && r < COLLISION_DISTANCE) {
					vx2 += ((sp1[tx][WEIGHT]* sp1[tx][VEL_X] - sp2[i][WEIGHT] *sp1[tx][VEL_X] + 2* sp2[i][WEIGHT]* sp2[i][VEL_X]) /
							(sp1[tx][WEIGHT] + sp2[i][WEIGHT])) - sp1[tx][VEL_X] ;
					vy2 += ((sp1[tx][WEIGHT]* sp1[tx][VEL_Y] - sp2[i][WEIGHT] *sp1[tx][VEL_Y] + 2* sp2[i][WEIGHT]* sp2[i][VEL_Y]) /
							(sp1[tx][WEIGHT] + sp2[i][WEIGHT])) - sp1[tx][VEL_Y] ;
					vz2 += ((sp1[tx][WEIGHT]* sp1[tx][VEL_Z] - sp2[i][WEIGHT] *sp1[tx][VEL_Z] + 2* sp2[i][WEIGHT]* sp2[i][VEL_Z]) /
							(sp1[tx][WEIGHT] + sp2[i][WEIGHT])) - sp1[tx][VEL_Z] ;
				}
			}
		}
	
		vx += vx2;
		vy += vy2;
		vz += vz2;
		pout.vel_x[idx] = vx;
		pout.vel_y[idx] = vy;
		pout.vel_z[idx] = vz;
		pout.pos_x[idx] = p.pos_x[idx] + dt * vx;
		pout.pos_y[idx] = p.pos_y[idx] + dt * vy;
		pout.pos_z[idx] = p.pos_z[idx] + dt * vz;
	}
	
}

/**
 * CUDA kernel to update particles
 * @param p       - particles
 * @param comX    - pointer to a center of mass position in X
 * @param comY    - pointer to a center of mass position in Y
 * @param comZ    - pointer to a center of mass position in Z
 * @param comW    - pointer to a center of mass weight
 * @param lock    - pointer to a user-implemented lock
 * @param N       - Number of particles
 */
__global__ void centerOfMass(t_particles p, float* comX, float* comY, float* comZ, float* comW, int* lock, const int N)
{
	const size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
	const int tx = threadIdx.x;
	
	extern __shared__ float sp[][4 + 1];
	float temp_w = 0;

	if(idx < N){
		sp[tx][0] = 0;
		sp[tx][1] = 0;
		sp[tx][2] = 0;
		sp[tx][3] = 0;
		for (unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; i < N; i += gridDim.x  * blockDim.x) {
			temp_w = ((float**)&p)[6][i];
			sp[tx][0] += ((float**)&p)[0][i] * temp_w;
			sp[tx][1] += ((float**)&p)[1][i] * temp_w;
			sp[tx][2] += ((float**)&p)[2][i] * temp_w;
			sp[tx][3] += temp_w;
		}
		__syncthreads();
		
		for (unsigned int stride = blockDim.x >> 1; stride >= 1; stride >>= 1) {
			if (threadIdx.x < stride) {
				sp[tx][0] += sp[threadIdx.x + stride][0];
				sp[tx][1] += sp[threadIdx.x + stride][1];
				sp[tx][2] += sp[threadIdx.x + stride][2];
				sp[tx][3] += sp[threadIdx.x + stride][3];
			}
			__syncthreads();
		}

		if(tx == 0) {
			atomicAdd(comX, sp[tx][0]);
			atomicAdd(comY, sp[tx][1]);
			atomicAdd(comZ, sp[tx][2]);
			atomicAdd(comW, sp[tx][3]);
		}
	}

}// end of centerOfMass
//----------------------------------------------------------------------------------------------------------------------

/**
 * CPU implementation of the Center of Mass calculation
 * @param particles - All particles in the system
 * @param N         - Number of particles
 */
__host__ float4 centerOfMassCPU(MemDesc& memDesc)
{
  float4 com = {0 ,0, 0, 0};

  for(int i = 0; i < memDesc.getDataSize(); i++)
  {
    // Calculate the vector on the line connecting current body and most recent position of center-of-mass
    const float dx = memDesc.getPosX(i) - com.x;
    const float dy = memDesc.getPosY(i) - com.y;
    const float dz = memDesc.getPosZ(i) - com.z;

    // Calculate weight ratio only if at least one particle isn't massless
    const float dw = ((memDesc.getWeight(i) + com.w) > 0.0f)
                          ? ( memDesc.getWeight(i) / (memDesc.getWeight(i) + com.w)) : 0.0f;

    // Update position and weight of the center-of-mass according to the weight ration and vector
    com.x += dx * dw;
    com.y += dy * dw;
    com.z += dz * dw;
    com.w += memDesc.getWeight(i);
  }
  return com;
}// enf of centerOfMassCPU
//----------------------------------------------------------------------------------------------------------------------
