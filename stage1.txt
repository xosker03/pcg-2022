5 * 512 * 10    Time: 3.676111 s
5 * 512 * 11    Time: 4.047395 s
5 * 512 * 12    Time: 4.415913 s
5 * 512 * 13    Time: 4.782001 s
5 * 512 * 14    Time: 5.142651 s
5 * 512 * 15    Time: 5.501910 s
5 * 512 * 16    Time: 5.862606 s
5 * 512 * 17    Time: 8.816829 s
5 * 512 * 18    Time: 9.333463 s
5 * 512 * 19    Time: 9.852422 s
5 * 512 * 20    Time: 10.374633 s
5 * 512 * 21    Time: 10.854354 s
5 * 512 * 22    Time: 11.357289 s
5 * 512 * 23    Time: 11.876155 s
5 * 512 * 24    Time: 12.399035 s
5 * 512 * 25    Time: 12.916433 s

Device "Tesla V100-SXM2-16GB (0)"
    Kernel: calculate_velocity(t_particles, t_particles, int, float)
          1                        flop_sp_efficiency                          FLOP Efficiency(Peak Single)      40.34%      40.34%      40.34%
          1                             sm_efficiency                               Multiprocessor Activity      87.13%      87.13%      87.13%
          1                          gld_transactions                              Global Load Transactions   512080000   512080000   512080000
          1                  shared_load_transactions                              Shared Load Transactions           0           0           0
          1                            gld_efficiency                         Global Memory Load Efficiency      12.51%      12.51%      12.51%
          1                          dram_utilization                             Device Memory Utilization     Low (1)     Low (1)     Low (1)
          1                            l2_utilization                                  L2 Cache Utilization     Low (1)     Low (1)     Low (1)
          1                   local_load_transactions                               Local Load Transactions           0           0           0
          1                           global_hit_rate                     Global Hit Rate in unified l1/tex      99.48%      99.48%      99.48%
          1                            local_hit_rate                                        Local Hit Rate       0.00%       0.00%       0.00%
          1                         shared_efficiency                              Shared Memory Efficiency       0.00%       0.00%       0.00%
          1                            gld_throughput                                Global Load Throughput  590.18GB/s  590.18GB/s  590.18GB/s
          1                             flop_count_sp           Floating Point Operations(Single Precision)  1.6384e+11  1.6384e+11  1.6384e+11
          1                         flop_count_sp_add       Floating Point Operations(Single Precision Add)  2.0480e+10  2.0480e+10  2.0480e+10
          1                         flop_count_sp_fma       Floating Point Operations(Single Precision FMA)  6.1439e+10  6.1439e+10  6.1439e+10
          1                         flop_count_sp_mul        Floating Point Operation(Single Precision Mul)  2.0480e+10  2.0480e+10  2.0480e+10
          1                     flop_count_sp_special   Floating Point Operations(Single Precision Special)  8191872000  8191872000  8191872000
[dd-21-22-133@cn195.barbora step1]$





























